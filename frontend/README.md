## FRONTEND 

##### Clone repo
> git clone git@gitlab.com:baronbrn/log_front.git
 

##### launch containers  
> docker-compose up --build

##### Access web interface
> http://localhost:4200

