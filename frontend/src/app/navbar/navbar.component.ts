import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input() isAuthenticated:any;
  @Input() token: any;
  username: any;

  accountDropDown:string[][]=[
    ['account','/account'],
    ['settings', '/settings'],
    ['logout', '/logout']
  ];

  constructor(
    private _router: Router
  ) { }

  ngOnInit(): void {
    if (localStorage.getItem('username')) 
      this.username=localStorage.getItem('username');
  }

  logout(){
    localStorage.removeItem('token');
    location.reload();
    this.goToLogin()
  }

  goToLogin() { this._router.navigate(['/login']); }
}
