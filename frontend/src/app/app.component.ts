import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/Model/User';
import { GetService } from './services/get.service';
import { PostService } from './services/post.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';
  isAuthenticated: boolean = false;
  token: string = '';
  username: string = '';

  constructor(
    private _getService: GetService,
    private _postService: PostService,
    private _router: Router
  ) { }

  ngOnInit() {
    //localStorage.clear();
    let bearer = localStorage.getItem('token');
    if (bearer) {
      this.isAuthenticated = true;
      this._getService.getMe(bearer).subscribe(
        (response: any) => {
          if (response.status === 200) {
            let refresh: string = localStorage.getItem('refreshToken');
            let user:User = this.initialiseUser(response.body, refresh);
                        
            localStorage.setItem('user', JSON.stringify(user));
          }
        },
        (error: any) => {
          if (error.error.code === 401) {
            let refreshToken: string = localStorage.getItem('refreshToken');

            this._postService.postRefreshToken(refreshToken).subscribe(
              (response: any) => {
                localStorage.setItem('token', response.body.token);
                localStorage.setItem('refreshToken', response.body.refresh_token);
              },
              (error: any) => {
                console.log('There is an error on refresh');
                console.log(error.error.message);
                localStorage.removeItem('token');
                location.reload();
                this._router.navigate(['/login']);
              }
            );
          }
        }
      );
    }
  }

  initialiseUser(body: any, refresh:string) {
    let usr = new User();
    usr.uuid = body.uuid;
    usr.email = body.email;
    usr.firstName = body.firstName;
    usr.lastName = body.lastName;
    usr.username = body.username;
    usr.country = body.country;
    usr.birthdate = body.birthdate;
    usr.phone = body.phone;
    usr.gender = body.gender;

    usr.balance = body.balance;
    usr.balanceFuture = body.balanceFuture;
    usr.balanceSpot = body.balanceSpot;

    usr.tradeInProgressSpot = body.tradeInProgressSpot;
    usr.tradeLostSpot = body.tradeLostSpot;
    usr.tradeNeutralSpot = body.tradeNeutralSpot;
    usr.tradeWonSpot = body.tradeWonSpot;

    usr.tradeInProgressFuture = body.tradeInProgressFuture;
    usr.tradeLostFuture = body.tradeLostFuture;
    usr.tradeNeutralFuture = body.tradeNeutralFuture;
    usr.tradeWonFuture = body.tradeWonFuture;

    usr.tradeInProgress = body.tradeInProgress;

    usr.refreshToken=refresh;

    return usr;
  }

  receiveToken($event: any) {
    this.token = $event;
    this.isAuthenticated = true;
  }
}
