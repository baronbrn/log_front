import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(public httpClient: HttpClient) { }

  serverUrl: string = 'https://localhost';

  _loginRoute: string=this.serverUrl+'/login';
  _refreshTokenRoute: string=this.serverUrl+'/token/refresh';

  _tradeRoute: string = this.serverUrl + '/trades';
  _setupRoute: string = this.serverUrl + '/setups';
  _categoryRoute: string = this.serverUrl + '/categories';
  _userRoute: string = this.serverUrl + '/users';
  _meRoute: string = this.serverUrl + '/me';
  _assetRoute: string = this.serverUrl + '/assets';
  _platformRoute: string = this.serverUrl + '/platforms';

  buildGetHeader(bearer: string|null) {
    return new HttpHeaders()
      .set('Authorization', 'Bearer ' + bearer);
  }

  buildPostHeader(bearer: string|null, contentType:string='application/ld+json') {
    return new HttpHeaders()
      .set('Content-Type', contentType)
      .set('Authorization', 'Bearer ' + bearer)
  }

  buildPostHeaderNotAuthentified() {
    return new HttpHeaders().set('Content-Type', 'application/json');
  }
}
