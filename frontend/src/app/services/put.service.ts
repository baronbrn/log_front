import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class PutService {

  constructor(
    private _apiService: ApiService
  ) { }

  put(bearer: string|null, body: string, path: string){
    return this._apiService.httpClient.put(
      path, body,
      {
        headers: bearer ? this._apiService.buildPostHeader(bearer) : this._apiService.buildPostHeaderNotAuthentified(),
        observe: 'response'
      }
    );
  }

  putTrade(bearer: string|null, body:string, tradeUuid:number){
    return this.put(bearer, body, this._apiService._tradeRoute+'/'+tradeUuid);
  }
}
