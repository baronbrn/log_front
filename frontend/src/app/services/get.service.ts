import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class GetService {

  constructor(
    private _apiService: ApiService
  ) { }

  get(bearer: string|null, route: string, uuid: string|null = null) {
    var path = route;
    if (uuid) { var path = route + '/' + uuid; }

    return this._apiService.httpClient.get(
      path,
      {
        headers: this._apiService.buildGetHeader(bearer),
        observe: 'response'
      }
    );
  }

  getPlatforms(bearer:string|null, uuid:string|null=null){
    return this.get(bearer, this._apiService._platformRoute, uuid);
  }
  getAssets(bearer: string | null, uuid: string | null = null) {
    return this.get(bearer, this._apiService._assetRoute, uuid);
  }
  getCategory(bearer: string | null, uuid: string | null = null) {
    return this.get(bearer, this._apiService._categoryRoute, uuid);
  }
  getSetups(bearer: string | null, uuid: string | null = null) {
    return this.get(bearer, this._apiService._setupRoute, uuid);
  }
  getTrades(bearer: string|null, filters:string|null=null, uuid: string|null = null) {
    return (filters === null) ?
      this.get(bearer, this._apiService._tradeRoute, uuid) :
      this.get(bearer, this._apiService.serverUrl+filters, uuid);
  }
  getMe(bearer: string | null) {
    return this.get(bearer, this._apiService._meRoute, null);
  }

  getPlatformTry(bearer:string|null, uuid:string|null=null){
    this.get(bearer, this._apiService._platformRoute, uuid).subscribe(
      (platforms:any) => {
        if (platforms.status === 200) {
          return platforms.body['hydra:member'];
        }
      }
    );
    return [];
  }
}
