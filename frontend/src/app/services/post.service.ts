import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(
    private _apiService: ApiService
  ) { }

  post(bearer: string|null, body: string, path: string){
    return this._apiService.httpClient.post(
      path, body,
      {
        headers: bearer ? this._apiService.buildPostHeader(bearer) : this._apiService.buildPostHeaderNotAuthentified(),
        observe: 'response'
      }
    );
  }

  postLogin(mail:string, password:string){
    return this.post(null, '{"username": "'+mail+'", "password": "'+password+'"}', this._apiService._loginRoute);
  }

  postRefreshToken(token:string) {    
    return this.post(null, '{"refresh_token": "'+token+'"}', this._apiService._refreshTokenRoute);
  }

  postTrade(bearer: string|null, body:string){
    return this.post(bearer, body, this._apiService._tradeRoute);
  }
  postSetup(bearer: string|null, body:string){
    return this.post(bearer, body, this._apiService._setupRoute);
  }
}
