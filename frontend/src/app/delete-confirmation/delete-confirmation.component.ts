import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';
import { HttpHeaders } from "@angular/common/http";

@Component({
  selector: 'app-delete-confirmation',
  templateUrl: './delete-confirmation.component.html',
  styleUrls: ['./delete-confirmation.component.css']
})
export class DeleteConfirmationComponent implements OnInit {

  constructor(
    public _apiService: ApiService,
    public _toastr :ToastrService,
    public modalRef: BsModalRef
  ) { }

  title:string;
  body: string;

  bearer: string|null;
  route: string;
  uuid: string;

  successMessage:string='Item deleted successfully.';
  errorMessage:string='Something went wrong. Contact support.';

  ngOnInit(): void {
  }

  delete() {
    this._apiService.httpClient.delete(
      this.route+'/'+this.uuid, { headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.bearer), observe: 'response' }
    ).subscribe((response: any) => {
      if (response.status === 204) {
        this._toastr.success(this.successMessage);
        window.location.reload();
      }
      else {
        this._toastr.warning(this.errorMessage);
      }
    })
  }
}
