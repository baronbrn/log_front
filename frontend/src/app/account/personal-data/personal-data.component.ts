import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.css']
})
export class PersonalDataComponent implements OnInit {

  constructor() { }

  disabled:boolean=true;
  isModifying:boolean=false;

  user:any;
  userGender:string;

  ngOnInit(): void {
    let user=localStorage.getItem('user');
    this.user=JSON.parse(user);
    this.userGender=this.user.gender;
    console.log(this.user);
  }

  modify(){
    this.isModifying = true;
    this.disabled=false;
  }

  save() {
    this.isModifying=false;
  }

}
