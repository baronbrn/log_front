import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DeleteConfirmationComponent } from '../delete-confirmation/delete-confirmation.component';
import { GetService } from '../services/get.service';

import {ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { TradeDetailComponent } from './trade-detail/trade-detail.component';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AddTradeComponent } from './add-trade/add-trade.component';
import { UpdateTradeComponent } from './update-trade/update-trade.component';


@Component({
  selector: 'trades',
  templateUrl: './trades.component.html',
  styleUrls: ['./trades.component.css']
})
export class TradesComponent implements OnInit {
  


  public trades: any;
  public bearer: string | null;
  modalRef: BsModalRef;
  categories:string[]=['All', 'Spot', 'Futures', 'Paper'];
  assets:string;
  statuses:string[]=['won', 'loss', 'neutral', 'in progress'];
  arrayOfArray: string[] = ['Type', 'Status'];



  tradesData:any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns:string[] = ['ind', 'asset', 'timeframe', 'riskReward', 'entry', 'stop', 'win', 'start', 'needed Input', 'status', 'type', 'category', 'result', 'actions']

  constructor(
    private _router: Router,
    private _apiService: ApiService,
    private _toastr: ToastrService,
    private modalService: BsModalService,
    private _getService: GetService,
    private dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    this.bearer = localStorage.getItem('token');
      
    this.bearer = localStorage.getItem('token');
    let currentUrl: string = this._router.url;

    if (currentUrl === '/trades') {
      this.getTradesWithFilter();
    } else {
      this.getTradesWithFilter(currentUrl);
    }  
  }

  getTradesWithFilter(currentUrl: string|null=null) {
    console.log(currentUrl);
    let rawTrades = this._getService.getTrades(this.bearer, currentUrl);
    rawTrades.subscribe((trades: any) => {
      if (trades.status === 200) {
        this.trades = trades.body['hydra:member'];
        this.tradesData = new MatTableDataSource<TradesInterface>(trades.body['hydra:member']);
        this.tradesData.paginator = this.paginator;
        
      } else {
        console.log('error status : ' + trades.status)
      }
    })
    return [];
  }

  detailTrade(index:number) {
    let tradeItem = this.trades[index];
    console.log(tradeItem);
    this.dialog.open(TradeDetailComponent, {
      data: { 'trade': tradeItem },
      height: '500px',
      width: '900px',
    })
  } 

  deleteTrade(uuid: any) {
    this.modalRef = this.modalService.show(DeleteConfirmationComponent, {
      initialState: {
        title: "Confirmation",
        body: 'Are you sure ?',
        bearer: this.bearer,
        route: this._apiService._tradeRoute,
        uuid: uuid
      }
    });
  }

  addTrade(){
    this.dialog.open(AddTradeComponent, {
      width:'900px'
    });
  }

  editTrade(index:number){
    let tradeItem = this.trades[index];
    console.log(tradeItem.id);
    this.dialog.open(UpdateTradeComponent, {
      width:'900px',
      data: { 'trade': tradeItem }
    })
  }
  
}

export interface TradesInterface {
  asset:string;
  setup:string;
  timeframe:string;
  entry:number;
  stop:number;
  win:number;
  start:string;
  neededInput:number;
  status:string;
  type:string;
  category:string;
}