import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Trade } from 'src/Model/Trade';
import { Asset } from 'src/Model/Asset';
import { Setup } from 'src/Model/Setup';
import { Platform } from 'src/Model/Platform';
import { ToastrService } from 'ngx-toastr';
import { GetService } from 'src/app/services/get.service';
import { Category } from 'src/Model/Category';
import { PutService } from 'src/app/services/put.service';
import { FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-update-trade',
  templateUrl: './update-trade.component.html',
  styleUrls: ['./update-trade.component.css']
})
export class UpdateTradeComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _toastr: ToastrService,
    private _getService: GetService,
    private _putService: PutService,
    private fb: FormBuilder
  ) { }

  updatedTrade: Trade;

  displayLeverage: boolean = false;
  riskRewardDefault: string = '';

  assetsArray: Asset[] = [];
  setupsArray: Setup[] = [];
  categoryArray: Category[] = [];
  PlatformArray: Platform[] = [];

  types: string[] = ['long', 'short'];
  timeframes: string[] = ['15m', '30m', '1h', '2h', '4h', '8h', '12h', '1D', '3D', '1W', '1M'];
  pairs: string[] = ['USDT', 'BTC', 'ETH'];
  statuses: string[] = ['win', 'loss', 'in progress', 'neutral'];

  bearer: string = "";

  tradeModel: Trade;

  get assetObject() { return this.getByKey('asset'); }
  get pair() { return this.getByKey('pair'); }
  get setup() { return this.getByKey('setup'); }
  get riskReward() { return this.getByKey('riskReward'); }
  get neededInput() { return this.getByKey('neededInput'); }
  get entry() { return this.getByKey('entry'); }
  get stop() { return this.getByKey('stop'); }
  get profit() { return this.getByKey('profit'); }
  get startDate() { return this.getByKey('startDate'); }
  get endDate() { return this.getByKey('endDate'); }
  get type() { return this.getByKey('type'); }
  get status() { return this.getByKey('status'); }
  get timeframe() { return this.getByKey('timeframe'); }
  get platform() { return this.getByKey('platform'); }
  get category() { return this.getByKey('category'); }
  get leverage() { return this.getByKey('leverage'); }
  get comment() { return this.getByKey('comment'); }


  tradeForm:any;

  getByKey(key: string) {
    return this.tradeForm.controls[key];
  }

  createFormControl(value: string = '') {
    return new FormControl(value);
  }

  ngOnInit(): void {
    this.bearer = localStorage.getItem('token');

    // ASSETS
    let rawAssets = this._getService.getAssets(this.bearer);
    rawAssets.subscribe((assets: any) => {
      if (assets.status === 200)
        this.assetsArray = assets.body['hydra:member'];
    });
    // SETUPS
    let rawSetups = this._getService.getSetups(this.bearer);
    rawSetups.subscribe((setups: any) => {
      if (setups.status === 200)
        this.setupsArray = setups.body['hydra:member'];
    });
    // CATEGORIES
    let rawCategories = this._getService.getCategory(this.bearer);
    rawCategories.subscribe((categories: any) => {
      if (categories.status === 200) {
        this.categoryArray = categories.body['hydra:member'];
      }
    });
    // PLATFORMS
    let rawPlatforms = this._getService.getPlatforms(this.bearer);
    rawPlatforms.subscribe(
      (platforms: any) => {
        if (platforms.status === 200)
          this.PlatformArray = platforms.body['hydra:member'];
      }, (error: any) => {
        this.errorMessage('fetching platforms');
        console.log(error);
      }
    );

    this.tradeModel = this.data.trade;

    this.tradeForm = this.fb.group({
      asset: new FormControl(this.tradeModel.asset.name),
      pair: new FormControl(this.tradeModel.pair),
      setup: new FormControl(this.tradeModel.setup.name),
      riskReward: new FormControl(this.tradeModel.riskReward),
      neededInput: new FormControl(this.tradeModel.neededInput),
      entry: new FormControl(this.tradeModel.entry),
      stop: new FormControl(this.tradeModel.stopLoss),
      profit: new FormControl(this.tradeModel.takeProfit),
      startDate: new FormControl(this.tradeModel.startDate),
      endDate: new FormControl(this.tradeModel.endDate),
      type: new FormControl(this.tradeModel.type),
      status: new FormControl(this.tradeModel.status),
      timeframe: new FormControl(this.tradeModel.timeframe),
      platform: new FormControl(this.tradeModel.platform.name),
      category: new FormControl(this.tradeModel.category.name),
      leverage: new FormControl(this.tradeModel.leverage),
      comment: new FormControl(this.tradeModel.comment),
    });

    if (this.tradeModel.category.name === 'Future') {
      this.displayLeverage = true;
    }
  }

  onCategoryChange() {
    let category = this.category;
    if (category['value'] === 'Future' && category['touched'] === true) {
      this.displayLeverage = true;
    } else {
      this.displayLeverage = false;
    }
  }

  errorMessage(message: string='') {
    this._toastr.warning('There has been an error ' + message);
  }

  onSetupChosen() {
    this.riskRewardDefault = this.tradeModel.setup.riskAllowed;
  }

  isObject(field: any) {
    return typeof field === 'object' && !Array.isArray(field) && field !== null;
  }


  submitEditTrade() {

    let trade: Trade = new Trade();
    trade._asset = this.assetObject;
    trade._pair = this.pair;
    trade._setup = this.setup;
    trade._riskReward = this.riskReward;
    trade._neededInput = this.neededInput;
    trade._entry = this.entry;
    trade._stopLoss = this.stop;
    trade._takeProfit = this.profit;
    trade._startDate = this.startDate;
    trade._endDate = this.endDate;
    trade._type = this.type;
    trade._status = this.status;
    trade._timeframe = this.timeframe;
    trade._platform = this.platform;
    trade._category = this.category;
    trade._leverage = this.leverage;
    trade._comment = this.comment;

    Object.keys(trade).forEach((key: any) => {
      if (key === 'asset') {
        this.assetsArray.forEach((asset: Asset) => {
          if (asset.name === trade._asset['value']) {
            trade._asset = asset['@id'];
          }
        });
      } else if (key === 'setup') {
        this.setupsArray.forEach((setup: Setup) => {
          if (setup.name === trade._setup['value']) {
            trade._setup = setup['@id'];
          }
        });
      } else if (key === 'category') {
        this.categoryArray.forEach((category:Category)=>{
          if (category.name === trade._category['value']) {
            trade._category = category['@id'];
          }
        });
      } else if (key === 'platform') {
        console.log('we get here : ' + key);
        console.log(trade._platform['value']);

        this.PlatformArray.forEach((platform:Platform)=>{
          if (platform.name === trade._platform['value']) {
            trade._platform = platform['@id'];
          }
        });
      } else {
        trade[key] = trade[key]['value'];
      }
    });

    this._putService.putTrade(this.bearer, JSON.stringify(trade), this.tradeModel.uuid)
    .subscribe(
      (response:any)=>{
        if (response.status === 200) {
          this._toastr.success('Trade modified !');
        }
      },
      (error:any)=>{
        this.errorMessage();
        console.log(error);
      }
    );
  }
}
