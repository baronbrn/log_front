import { Component, OnInit } from '@angular/core';
import { Trade } from 'src/Model/Trade';
import { Asset } from 'src/Model/Asset';
import { Setup } from 'src/Model/Setup';
import { Platform } from 'src/Model/Platform';
import { Category } from 'src/Model/Category';
import { ToastrService } from 'ngx-toastr';
import { GetService } from 'src/app/services/get.service';
import { PostService } from 'src/app/services/post.service';

@Component({
  selector: 'app-add-trade',
  templateUrl: './add-trade.component.html',
  styleUrls: ['./add-trade.component.css']
})
export class AddTradeComponent implements OnInit {

  constructor(
    private _toastr: ToastrService,
    private _getService: GetService,
    private _postService: PostService
  ) { }

  displayLeverage: boolean = false;
  riskRewardDefault: string = '';

  tradeModel: Trade = new Trade();
  assetsArray: Asset[] = [];
  setupsArray: Setup[] = [];
  categoryArray: Category[] = [];
  PlatformArray: Platform[] = [];

  types: string[] = ['long', 'short'];
  timeframes: string[] = ['15m', '30m', '1h', '2h', '4h', '8h', '12h', '1D', '3D', '1W', '1M'];
  pairs: string[] = ['USDT', 'BTC', 'ETH'];
  statuses: string[] = ['win', 'loss', 'in progress', 'neutral'];

  bearer:any;

  ngOnInit(): void {

    this.bearer = localStorage.getItem('token');

    // ASSETS
    let rawAssets = this._getService.getAssets(this.bearer);
    rawAssets.subscribe((assets: any) => {
      if (assets.status === 200)
        this.assetsArray = assets.body['hydra:member'];
    });
    // SETUPS
    let rawSetups = this._getService.getSetups(this.bearer);
    rawSetups.subscribe((setups: any) => {
      if (setups.status === 200)
        this.setupsArray = setups.body['hydra:member'];
    });
    // CATEGORIES
    let rawCategories = this._getService.getCategory(this.bearer);
    rawCategories.subscribe((categories: any) => {
      if (categories.status === 200) {
        this.categoryArray = categories.body['hydra:member']
        //this.categoryArray = this.assignCategories(categories.body['hydra:member'])
      }
    });
    // PLATFORMS
    let rawPlatforms = this._getService.getPlatforms(this.bearer);
    rawPlatforms.subscribe((platforms: any) => {
      if (platforms.status === 200)
        this.PlatformArray = platforms.body['hydra:member'];
    });

    this.tradeModel.status = 'in progress';
    this.tradeModel.pair='USDT';
  }

  assignCategories(categories: Array<Category>) {
    let arrayOfCategories = [];
    categories.forEach((category: any) => {
      let cat: Category = new Category();
      cat.id = category.id;
      cat.uuid = category.uuid;
      cat.name = category.name;
      arrayOfCategories.push(cat);
    })
    return arrayOfCategories;
  }

  // Might have to be changed when implementing paper trade
  onCategoryChange() {
    this.categoryArray.forEach((element: any) => {
      if (element.name === 'Future') {
        // get Uuid from chosen category. Should update to cleaner code
        let uuidChosenCategory = (JSON.stringify(this.tradeModel.category).substring(13)).slice(0, -1);
        if (element.uuid === uuidChosenCategory) {
          this.displayLeverage = true;
        }
      } else {
        this.displayLeverage = false;
        this.tradeModel.leverage = null;
      }
    });
  }

  // Pre fill riskReward for user
  onSetupChosen() {
    let uuidChosenSetup = (JSON.stringify(this.tradeModel.setup).substring(9)).slice(0, -1);
    this.setupsArray.forEach((element: any) => {
      if (element.uuid == uuidChosenSetup) {
        this.riskRewardDefault = element.riskAllowed;
      }
    });
  }

  submitTrade() {
    let user = JSON.parse(localStorage.getItem('user'));

    this.tradeModel.trader = '/users/' + user.uuid;


    console.log(JSON.stringify(this.tradeModel));

    this.computePossibleWinAndLoss();

    console.log(JSON.stringify(this.tradeModel));

    /*
    this._postService.postTrade(
      this.bearer,
      JSON.stringify(this.tradeModel)
    ).subscribe(
      (trade: any) => {
        if (trade.status === 201) {
          this._toastr.success('Trade added!');
          setTimeout(() => { location.reload(); }, 3000);
        } else {
          this.errorMessage();
        }
      },
      (error: any) => {
        if (error.status===422){
          error.error.violations.forEach(element => {
            this.errorMessage(element.message);
          });
        } else {
          this.errorMessage();
        }
      }
    );*/
  }

  errorMessage(message:string='Something happened contact support'){
    this._toastr.warning(message);
  }

  computePossibleWinAndLoss() {
    let assetAmount: number = this.tradeModel.neededInput / this.tradeModel.entry;;
    if (this.tradeModel.stopLoss === null) {
      this.tradeModel.win=1;
      this.tradeModel.loss=1;
    } else {
      if (this.tradeModel.type === 'long') {
        this.tradeModel.win = assetAmount * this.tradeModel.takeProfit;
        this.tradeModel.loss = this.tradeModel.neededInput - (assetAmount * this.tradeModel.stopLoss);
      }
      if (this.tradeModel.type === 'short') {
        this.tradeModel.win = 1;
        this.tradeModel.loss = 2;
      }
    }
  }
}
