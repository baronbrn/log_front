import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PostService } from '../services/post.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private _router: Router,
    private _toastr: ToastrService,
    private _postService: PostService
  ) { }

  @Output() event = new EventEmitter<string>();

  mail: string = '';
  password: string = '';

  hide:boolean=true;
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);

  ngOnInit(): void {
    localStorage.clear();
    if (localStorage.getItem('token') !== null)
      this._router.navigate(['/dashboard']);
  }

  login() {
    if (this.mail && this.password) {
      this._postService.postLogin(this.mail, this.password).subscribe(
        (response: any) => {
          if (response.status === 200)
            this.onLoginSuccess(response);
        },
        (error: any) => {
          this.onLoginFailure(error.error.message);
        }
      );
    }
  }

  onLoginSuccess(data: any) {
    this._toastr.success('Welcome!');

    var token: string = data.body.token;
    this.event.emit(token);

    localStorage.setItem('token', token);
    localStorage.setItem('refreshToken', data.body.refresh_token);


    setTimeout(() => {
      this._router.navigate(['/dashboard']);
    }, 3000)
  }

  onLoginFailure(message: string) {
    this._toastr.warning();
  }

  onSignUpButton() {
    console.log('sign up button coming soon');
  }
}
