import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Trade } from 'src/Model/Trade';
import {MatChipInputEvent} from '@angular/material/chips';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { TradeDetailComponent } from '../trades/trade-detail/trade-detail.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(
    private _router: Router,
    private dialog: MatDialog
  ) { }

  wallet: number;
  spotWallet: number;
  futureWallet: number;

  wonSpot: number;
  inProgressSpot: number;
  lostSpot: number;
  neutralSpot: number;

  wonFuture: number;
  inProgressFuture: number;
  lostFuture: number;
  neutralFuture: number;

  ongoingTrades:Array<Trade>=[];

  parsed:any;

  ngOnInit(): void {

    let usrStr = localStorage.getItem('user');
    this.parsed = JSON.parse(usrStr!);

    console.log(this.parsed);

    this.wallet = this.parsed.balance;
    this.spotWallet = this.parsed.balanceSpot;
    this.futureWallet = this.parsed.balanceFuture;

    this.wonSpot = this.parsed.tradeWonSpot;
    this.inProgressSpot = this.parsed.tradeInProgressSpot;
    this.lostSpot = this.parsed.tradeLostSpot;
    this.neutralSpot =this.parsed.tradeNeutralSpot;

    this.wonFuture = this.parsed.tradeWonFuture;
    this.inProgressFuture = this.parsed.tradeInProgressFuture;
    this.lostFuture = this.parsed.tradeLostFuture;
    this.neutralFuture = this.parsed.tradeNeutralFuture;

    this.parseTrade();

    
  }

  parseTrade(){
    //this.ongoingTrades = this.parsed.tradeInProgress;
    //console.log(this.ongoingTrades);

    Object.keys(this.parsed.tradeInProgress).forEach(key => {
      this.ongoingTrades.push(this.instanciateTrade(this.parsed.tradeInProgress[key]))
    })
    console.log(this.ongoingTrades);
  }

  instanciateTrade(item:any){
    let trade: Trade = new Trade();

    trade.id=item.id;
    trade.type=item.type;
    trade.entry=item.entry;
    trade.stopLoss=item.stopLoss;
    trade.takeProfit=item.takeProfit;
    
    trade.leverage=item.leverage;
    trade.pair=item.pair;
    trade.neededInput=item.neededInput;
    trade.progression = item.progression;
    trade.timeframe=item.timeframe;
    trade.setup=item.setup;
    trade.asset=item.asset;
    trade.category=item.category;

    return trade;
  }
  
  tradeDetail(id:any){
    let tradeItem:Trade;
    this.ongoingTrades.forEach(element => {
      if (element.id == id) {
        tradeItem = element;
      }
    });
    tradeItem.status='In progress';
    this.dialog.open(TradeDetailComponent, {
      data:{'trade':tradeItem},
      height:'500px',
      width:'900px'
    })
  }

  goToTrade(status: string, categoryName: string) {
    this._router.navigate(
      ['/trades'],
      {
        queryParams:
          { status: status, 'category.name': categoryName }
      }
    );
  }

}
