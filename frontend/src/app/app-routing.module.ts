import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountComponent } from './account/account.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { AddSetupComponent } from './setups/add-setup/add-setup.component';
import { SetupsComponent } from './setups/setups.component';
import { AddTradeComponent } from './trades/add-trade/add-trade.component';
import { TradesComponent } from './trades/trades.component';

const routes: Routes = [
    {path: 'trades', component: TradesComponent},
    {path: 'setups', component: SetupsComponent},
    {path: 'dashboard', component: DashboardComponent},
    {path: 'add/setup', component: AddSetupComponent},
    {path: 'add/trade', component: AddTradeComponent},
    {path: 'login', component: LoginComponent},
    {path: 'account', component: AccountComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
