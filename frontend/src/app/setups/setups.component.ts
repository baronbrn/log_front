import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DeleteConfirmationComponent } from '../delete-confirmation/delete-confirmation.component';
import { GetService } from '../services/get.service';
import { SetupDetailComponent } from './setup-detail/setup-detail.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { AddSetupComponent } from './add-setup/add-setup.component';



@Component({
  selector: 'app-setups',
  templateUrl: './setups.component.html',
  styleUrls: ['./setups.component.css']
})
export class SetupsComponent implements OnInit {

  public setups: any;
  public bearer: string | null;
  public showTrue: boolean = false;

  modalRef: BsModalRef;

  setupData: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['name', 'description', 'actions'];

  constructor(
    private _apiService: ApiService,
    private _getService: GetService,
    private _toastr: ToastrService,
    private _router: Router,
    private modalService: BsModalService,
    private _dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.bearer = localStorage.getItem('token');

    let rawSetup = this._getService.getSetups(this.bearer);

    rawSetup.subscribe((setups: any) => {
      if (setups.status === 200) {
        this.setups = setups.body['hydra:member'];
        this.setupData = new MatTableDataSource<SetupInterface>(setups.body['hydra:member']);
      }
    });
  }

  setupDetail(index: number) {
    let setup = this.setups[index];
    this.modalRef = this.modalService.show(SetupDetailComponent, {
      initialState: {
        setupName: setup.name,
        description: setup.description,
        rules: this.setups[index]['rules']
      }
    })
  }

  deleteSetup(uuid: string) {
    this.showTrue = true;

    this.modalRef = this.modalService.show(DeleteConfirmationComponent, {
      initialState: {
        title: 'Confirmation',
        body: 'Are you sure?',
        bearer: this.bearer,
        route: this._apiService._setupRoute,
        uuid: uuid
      }
    });
  }

  goToAddSetup() {
    this._router.navigate(['/add/setup'])
  }

  openAddSetup(){
    this._dialog.open(AddSetupComponent, {
      height: '800px',
      width: '900px'
    });
  }
}

export interface SetupInterface {
  name: string;
  description:string;
}
