import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl } from '@angular/forms';
import { Setup } from 'src/Model/Setup'
import { Rules } from 'src/Model/Rules';
import { PostService } from 'src/app/services/post.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-setup',
  templateUrl: './add-setup.component.html',
  styleUrls: ['./add-setup.component.css']
})
export class AddSetupComponent implements OnInit {


  ruleForm = this.generateRuleForm();

  setupForm = this.fb.group({
    setupName: this.createFormControl(),
    setupDescription: this.createFormControl(),
    arrayOfRules: this.fb.array([
      this.ruleForm
    ])
  });

  constructor(
    private _toastr: ToastrService,
    private _postService: PostService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
  }

  createFormControl() {
    return new FormControl('');
  }

  generateRuleForm() {
    return this.fb.group({
      ruleName: this.createFormControl(),
      ruleContent: this.createFormControl()
    });
  }

  get arrayOfRules() {
    return this.setupForm.controls['arrayOfRules'] as FormArray;
  }

  get nameSetup() {
    return this.setupForm.controls['setupName'].value;
  }
  get descriptionSetup() {
    return this.setupForm.controls['setupDescription'].value;
  }

  addRule() {
    this.setupForm.controls['arrayOfRules'].push(this.generateRuleForm());
  }

  removeRule(idx: number) {
    this.arrayOfRules.removeAt(idx);
  }


  public submit() {
    let bearer = localStorage.getItem('token');

    let user = JSON.parse(localStorage.getItem('user'));

    let rules: Array<Rules> = [];
    this.arrayOfRules.controls.forEach((element: any) => {
      let rule = new Rules();
      rule._name = element.value.ruleName;
      rule._content = element.value.ruleContent;
      rules.push(rule);
    });


    let setup = new Setup();
    setup._name = this.nameSetup;
    setup._description = this.descriptionSetup;
    setup._rules = rules;
    setup._trader = '/users/' + user.uuid;

    this._postService.postSetup(bearer, JSON.stringify(setup)).subscribe(
      (response: any) => {
        if (response.status === 201) {
          this._toastr.success('Setup ' + setup._name + ' created!');
          setTimeout(() => { location.reload(); }, 3000)
        }
      },
      (error: any) => {
        if (error.status === 422) {
          for (let violation of error.error.violations) {
            this._toastr.warning(violation.message);
          }
        } else {
          this._toastr.warning('An error happened, contact support');
        }
      }
    )
  }
}
