import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Rules } from 'src/Model/Rules';

@Component({
  selector: 'app-setup-detail',
  templateUrl: './setup-detail.component.html',
  styleUrls: ['./setup-detail.component.css']
})
export class SetupDetailComponent implements OnInit {

  constructor(
    public modalRef: BsModalRef
  ) { }

  setupName: string;
  description: string;
  rules: Rules[];

  ngOnInit(): void {
  }

}
