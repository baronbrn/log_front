import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Setup } from 'src/Model/Setup'
import { Rules } from 'src/Model/Rules';
import { Router } from '@angular/router'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    public httpClient: HttpClient,
    private _router: Router
  ) {}

  serverUrl: string = 'https://localhost';

  post(
    url: string,
    content: string,
    redirect: boolean=true
  ) {
    this.httpClient.post(
      this.serverUrl+url, 
      content, 
      {headers: new HttpHeaders().set('Content-type', 'application/ld+json'),observe: 'response'}
    ).subscribe(resp => {
      var parsedData=resp as any;
      if(parsedData.status===201) {
        if (redirect)
          setTimeout(()=>{ this._router.navigate([url]); }, 5000)
        else
          return parsedData;
      } else {
        console.log('Something went wrong : ' + parsedData);
      }
    });
  }

  postSetup(setup: Setup, redirect: boolean=true){
    this.post('/setups', JSON.stringify(setup), redirect);
  }

  postRule(rule: Rules, redirect: boolean=true) {
    this.post('/rules', JSON.stringify(rule), redirect)
  }



  postTrade() {
    console.log('POST TRADE FUNCTION');
  }

}
