import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddPlatformComponent } from './add-platform/add-platform.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TradesComponent } from './trades/trades.component';
import { SetupsComponent } from './setups/setups.component';
import { AddSetupComponent } from './setups/add-setup/add-setup.component';
import { AddTradeComponent } from './trades/add-trade/add-trade.component';
import { LoginComponent } from './login/login.component';

import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardComponent } from './dashboard/dashboard.component';

import { ModalModule } from 'ngx-bootstrap/modal';
import { DeleteConfirmationComponent } from './delete-confirmation/delete-confirmation.component';
import { SetupDetailComponent } from './setups/setup-detail/setup-detail.component';


import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatTabsModule } from '@angular/material/tabs';

import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { AccountComponent } from './account/account.component';

import { MatInputModule } from '@angular/material/input';
import { PersonalDataComponent } from './account/personal-data/personal-data.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { TradeDetailComponent } from './trades/trade-detail/trade-detail.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import { UpdateTradeComponent } from './trades/update-trade/update-trade.component';

@NgModule({
  declarations: [
    AppComponent,
    AddPlatformComponent,
    SidebarComponent,
    NavbarComponent,
    TradesComponent,
    SetupsComponent,
    AddSetupComponent,
    AddTradeComponent,
    LoginComponent,
    DashboardComponent,
    DeleteConfirmationComponent,
    SetupDetailComponent,
    AccountComponent,
    PersonalDataComponent,
    TradeDetailComponent,
    UpdateTradeComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    MatCardModule,
    MatChipsModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
