import { Setup } from "./Setup"

export class Rules {
    private id: number
    private uuid: number
    private name: string
    private content: string
    private setup: Setup

    get _id(){ return this.id; }
    get _uuid(){ return this.uuid; }

    get _name(){ return this.name; }
    set _name(name:string){ this.name=name; }

    get _content(){ return this.content; }
    set _content(content:string){ this.content=content; }

    get _setup(){ return this.setup; }
}