import { Rules } from "./Rules"

export class Setup {
    private id: number
    private uuid: number
    public name: string
    private description: string
    private rules: Array<Rules>
    private _riskAllowed:string
    private favortie:boolean
    private numberOfTrade:number
    private trader: string;

    public get _id(){ return this.id; }
    public get _uuid(){ return this.uuid; }

    public get _name(){ return this.name; }
    public set _name(name:string){this.name=name;}

    public get _description(){ return this.description; }
    public set _description(description:string){ this.description=description;}

    public get _rules(){ return this.rules; }
    public set _rules(rules: Array<Rules>){ this.rules=rules;}

    public get riskAllowed(){ return this._riskAllowed; }
    public set riskAllowed(riskAllowed:string){this.riskAllowed=riskAllowed;}

    public get _favorite(){ return this.favortie; }
    public set _favorite(favorite:boolean){this.favortie=favorite;}

    public get _numberOfTrades(){ return this.numberOfTrade; }

    public get _trader(){return this.trader;}
    public set _trader(trader:string){this.trader=trader;}
}
