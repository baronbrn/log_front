export class Platform {
    public id: number
    public uuid: number
    public name: string
    public link: string
}