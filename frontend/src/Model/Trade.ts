import { Setup } from "./Setup";
import { Asset } from "./Asset"
import { Category } from "./Category"
import { Platform } from "./Platform";

export class Trade {
    id: number
    uuid: number
    entry: number
    stopLoss: number
    takeProfit: number
    neededInput: number
    type: string    
    timeframe: string
    startDate: Date
    endDate: Date
    win: number
    loss: number
    asset: Asset
    category: Category
    walletBalance: number
    progression: number
    riskReward: number
    leverage:number;
    pair:string;
    platform:Platform;
    passed: boolean
    status: string
    trader: string
    setup: Setup
    comment:string


    public get _id(){ return this.id; }
    public get _uuid(){ return this.uuid; }
    
    public get _entry(){ return this.entry}
    public set _entry(_newEntry:number){this.entry=_newEntry;}

    public get _stopLoss(){return this.stopLoss;}
    public set _stopLoss(_newStop: number){this.stopLoss=_newStop;}

    public get _takeProfit(){return this.takeProfit;}
    public set _takeProfit(_newTp: number){this.takeProfit=_newTp;}

    public get _neededInput(){return this.neededInput;}
    public set _neededInput(_newNeededInput: number){this.neededInput=_newNeededInput;}

    public get _type(){ return this.type;}
    public set _type(_newType: string){this.type=_newType;}

    public get _timeframe(){ return this.timeframe;}
    public set _timeframe(_newTf: string){this.timeframe=_newTf;}

    public get _startDate(){ return this.startDate;}
    public set _startDate(_newStartDate: Date){this.startDate=_newStartDate;}

    public get _endDate(){ return this.endDate;}
    public set _endDate(_newEndDate: Date){this.endDate=_newEndDate;}

    public get _win(){ return this.win;}
    public set _win(_newWin: number){this.win=_newWin;}

    public get _loss(){ return this.loss;}
    public set _loss(_newLoss: number){this.loss=_newLoss;}

    public get _asset(){ return this.asset;}
    public set _asset(_newAsst:Asset){this.asset=_newAsst;}

    public get _category(){ return this.category;}
    public set _category(_newCategory:Category){this.category=_newCategory;}

    public get _walletBalance(){ return this.walletBalance;}
    public set _walletBalance(_newWalletBalance:number){this.walletBalance=_newWalletBalance;}

    public get _progression(){ return this.progression;}
    public set _progression(_newProgression:number){this.progression=_newProgression;}

    public get _riskReward(){ return this.riskReward;}
    public set _riskReward(_newRR:number){this.riskReward=_newRR;}

    public get _leverage(){ return this.leverage;}
    public set _leverage(_newLeverage:number){this.leverage=_newLeverage;}

    public get _pair(){ return this.pair;}
    public set _pair(_newPair:string){this.pair=_newPair;}

    public get _platform(){ return this.platform;}
    public set _platform(_newPlatform:Platform){this.platform=_newPlatform;}

    public get _passed(){ return this.passed;}
    public set _passed(_newPassed:boolean){this.passed=_newPassed;}

    public get _status(){ return this.status;}
    public set _status(_newStatus:string){this.status=_newStatus;}

    public get _trader(){ return this.trader;}
    public set _trader(_newTrader:string){this.trader=_newTrader;}

    public get _setup(){ return this.setup;}
    public set _setup(_newSetup:Setup){this.setup=_newSetup;}

    public get _comment(){ return this.comment;}
    public set _comment(_newComment:string){this.comment=_newComment;}
}