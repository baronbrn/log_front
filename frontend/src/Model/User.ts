import { Setup } from "./Setup";
import { Trade } from "./Trade";

export class User {
    id: number
    uuid: number
    
    email: string
    roles: Array<string>
    password: string
    trades: Array<Trade>
    setups: Array<Setup>
    wallets:string // change that
    
    username: string
    birthdate: string
    firstName: string
    lastName: string
    phone: string
    country: string
    gender: string
    balance:number
    balanceFuture:number
    balanceSpot:number

    tradeInProgressSpot: number
    tradeLostSpot: number
    tradeNeutralSpot: number
    tradeWonSpot: number

    tradeInProgressFuture: number
    tradeLostFuture: number
    tradeNeutralFuture: number
    tradeWonFuture: number

    tradeInProgress: Array<string>

    refreshToken: string;
}