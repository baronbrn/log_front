export class Asset {
    id: number
    uuid: number
    name: string
    currentPrice: number
    symbol: string
}